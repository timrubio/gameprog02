// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Lab4Comp.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GP02_API ULab4Comp : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	ULab4Comp();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AActor* parent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<AActor*> children;

	FTransform LocalTransform;

	FTransform CurrentTransform;

	void SetTranslation(FTransform t, FVector v);

	void SetRotation(FTransform t, FVector v);

	void SetScale(FTransform t, FVector v);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
