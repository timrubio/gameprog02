// Fill out your copyright notice in the Description page of Project Settings.


#include "MyActor.h"

// Sets default values
AMyActor::AMyActor() : speed(100)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Anything that's a C++ class must use CreateDefaultSubobject
	TheMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	TheMesh->SetupAttachment(RootComponent);

	//For everything that's an object, we use FObjectFinder, here we create static mesh for a cone
	static ConstructorHelpers::FObjectFinder<UStaticMesh>
		ConeVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cone"));

	if (ConeVisualAsset.Succeeded())
	{
		TheMesh->SetStaticMesh(ConeVisualAsset.Object);
		TheMesh->SetRelativeLocation(FVector(0.0, 0.0, 0.0));
	}

	//Create material to put on cone
	static ConstructorHelpers::FObjectFinder<UMaterialInterface>
		ConeMaterial(TEXT("/Game/StarterContent/Materials/M_Cobblestone_Pebble"));

	if (ConeMaterial.Succeeded())
	{
		TheMesh->SetMaterial(0, ConeMaterial.Object);
	}

}

// Called when the game starts or when spawned
void AMyActor::BeginPlay()
{
	Super::BeginPlay();
	
	InitialPosition = GetActorLocation();
	Target = EndPosition;
}

// Called every frame
void AMyActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//If we have reached target, switch target so we can move to next point
	if (GetActorLocation().Equals(Target))
	{
		if (Target.Equals(EndPosition))
			Target = InitialPosition;
		else
			Target = EndPosition;
	}
	else
	{
		FVector nextPos = AMyActor::MoveTowards(GetActorLocation(), Target, speed * DeltaTime);
		SetActorLocation(nextPos);
	}
}

FVector AMyActor::MoveTowards(FVector CurrentLocation, FVector Destination, float MaxDistanceDelta)
{
	FVector towards = Destination - CurrentLocation;

	//Compare the square distance
	float sqDist = towards.SizeSquared();
	float sqMax = MaxDistanceDelta * MaxDistanceDelta;

	if (sqDist < sqMax)
	{
		return Destination;
	}
	else
	{
		//Returns a unit vector
		towards.Normalize();
		return CurrentLocation + (towards * MaxDistanceDelta);
	}
}

