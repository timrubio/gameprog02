// Fill out your copyright notice in the Description page of Project Settings.


#include "SceneH.h"

// Sets default values
ASceneH::ASceneH()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASceneH::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASceneH::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

