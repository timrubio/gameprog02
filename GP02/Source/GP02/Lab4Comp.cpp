// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab4Comp.h"

// Sets default values for this component's properties
ULab4Comp::ULab4Comp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	RegisterComponent();
}


// Called when the game starts
void ULab4Comp::BeginPlay()
{
	Super::BeginPlay();
	if (GetOwner())
	{
		LocalTransform = GetOwner()->GetActorTransform();
	}
	else
	{
		LocalTransform = parent->GetActorTransform();
	}
	CurrentTransform = LocalTransform; //Holds owner location to see if changed
}


// Called every frame
void ULab4Comp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (children.Num() > 0 && !(GetOwner()->GetActorTransform().Equals(CurrentTransform)))
	{
		for (int i = 0; i < children.Num(); i++)
		{
			children[i]->SetActorTransform(children[i]->GetActorTransform() * GetOwner()->GetActorTransform());
		}
		CurrentTransform = GetOwner()->GetActorTransform();
	}
}

void ULab4Comp::SetTranslation(FTransform t, FVector v)
{
	LocalTransform.SetLocation(v);
}

void ULab4Comp::SetRotation(FTransform t, FVector v)
{
	FQuat q = FQuat(v, 1);
	LocalTransform.SetRotation(q);
}

void ULab4Comp::SetScale(FTransform t, FVector v)
{
	LocalTransform.SetScale3D(v);
}

